## Layout Conventions
Good layout uses formatting to emphasize the structure of your code and to make the code easier to read. Microsoft examples and samples conform to the following conventions:
Use the default Code Editor settings (smart indenting, four-character indents, tabs saved as spaces).
- Write only one statement per line.
- Write only one declaration per line.
- If continuation lines are not indented automatically, indent them one tab stop (four spaces).
- Add at least one blank line between method definitions and property definitions.
- Use parentheses to make clauses in an expression apparent, as shown in the following code.
```c#
if ((val1 > val2) && (val1 > val3))
{
    // Take appropriate action.
}
```

## Commenting Conventions
- Place the comment on a separate line, not at the end of a line of code.
- Begin comment text with an uppercase letter.
- End comment text with a period.
- Insert one space between the comment delimiter (//) and the comment text, as shown in the following example.
```c#
// The following declaration creates a query. It does not run
// the query.
```

## Name Scheme
Naming needs to stay consistent, otherwise, it will very difficult to find things inside the document.
There are two main ways to name things:
- camelCase: which means that every word of the file name is capitalized except for the first one, e.g. nameFilesConsistently
- Underscores: in this case you write underscores between each word, e.g. name_files_consistently

## Variable Declaration
### Naming Variable
#### Before
```c#
var query = (from s in db.tbl_employee where s.pk_id == id select s).ToList();
```
#### After
```c#
var employeeData = (from s in db.tbl_employee where s.pk_id == id select s).ToList();
```
A good variable name should be clear and unambiguous.

### Data Type
#### Before
```c#
var greeting = "Hello World!";
```
#### After
```c#
string greeting = "Hello World!";
```
It's better to define the variable data type 

## String Comparasion
#### Before
```c#
string name;
if(name == null){
  //logic
}
```
#### After
```c#
string name;
if(String.IsNullOrEmpty(name)){
  //logic
}
```
Its better to use IsNullOrEmpty function for string comparasion instead of ==

## && and || Operators
To avoid exceptions and increase performance by skipping unnecessary comparisons, use && instead of & and || instead of | when you perform comparisons, as shown in the following example.
```c#
Console.Write("Enter a dividend: ");
var dividend = Convert.ToInt32(Console.ReadLine());

Console.Write("Enter a divisor: ");
var divisor = Convert.ToInt32(Console.ReadLine());

// If the divisor is 0, the second clause in the following condition
// causes a run-time error. The && operator short circuits when the
// first expression is false. That is, it does not evaluate the
// second expression. The & operator evaluates both, and causes
// a run-time error when divisor is 0.
if ((divisor != 0) && (dividend / divisor > 0))
{
    Console.WriteLine("Quotient: {0}", dividend / divisor);
}
else
{
    Console.WriteLine("Attempted division by 0 ends up here.");
}
```

## Object Declaration
- Use the concise form of object instantiation, with implicit typing, as shown in the following declaration.
```c#
var instance1 = new ExampleClass();
```
The previous line is equivalent to the following declaration.
```c#
ExampleClass instance2 = new ExampleClass();
```

- Use object initializers to simplify object creation.
```c#
// Object initializer.
var instance3 = new ExampleClass { Name = "Desktop", ID = 37414,
    Location = "Redmond", Age = 2.3 };

// Default constructor and assignment statements.
var instance4 = new ExampleClass();
instance4.Name = "Desktop";
instance4.ID = 37414;
instance4.Location = "Redmond";
instance4.Age = 2.3;
```

## Null Posibility Check
#### Before
```c#
public ActionResult EditEmployeeName(int id, string name)
{

	var employee = db.tbl_employee.Where(x => x.pk_id == id).FirstOrDefault();
	employee.name = name;
	db.Entry(employee).State = EntityState.Modified;
	db.SaveChanges();

}
```
#### After
```c#
public ActionResult EditEmployeeName(int id, string name)
{
  
	if (id == null)
	{
		throw new Exception("ID Parameter not Found");
	}

	if (String.IsNullOrEmpty(name))
	{
		throw new Exception("Name Parameter not Found");
	}

	var employee = db.tbl_employee.Where(x => x.pk_id == id).FirstOrDefault();
	employee.name = name;
	db.Entry(employee).State = EntityState.Modified;
	db.SaveChanges();

}
```
We need to check null posibility to avoid null reference exception

## LINQ Queries
- Use meaningful names for query variables. The following example uses jakartaEmployees for employee who are located in Jakarta.
```c#
var jakartaEmployee = from employee in tbl_employee
                       where employee.City == "Jakarta"
                       select employee.Name;
```

- Use where clauses before other query clauses to ensure that later query clauses operate on the reduced, filtered set of data.
```c#
var jakartaEmployee = from employee in tbl_employee
                       where employee.City == "Jakarta"
                       select employee.Name;
```

- Rename properties when the property names in the result would be ambiguous. For example, if your query returns a customer name and a distributor ID, instead of leaving them as Name and ID in the result, rename them to clarify that Name is the name of a customer, and ID is the ID of a distributor.
```c#
var localDistributors2 =
    from customer in customers
    join distributor in distributors on customer.City equals distributor.City
    select new { CustomerName = customer.Name, DistributorID = distributor.ID };
```

- Align query clauses under the from clause, as shown in the previous examples.
- Use multiple from clauses instead of a join clause to access inner collections. For example, a collection of Student objects might each contain a collection of test scores. When the following query is executed, it returns each score that is over 90, along with the last name of the student who received the score.
```c#
// Use a compound from to access the inner sequence within each element.
var scoreQuery = from student in students
                 from score in student.Scores
                 where score > 90
                 select new { Last = student.LastName, score };
```

## Try Catch
#### Before

```c#
public ActionResult EditDataEmployee(tbl_employee employee)
{	
	if(employee == null){
		throw new Exception("Employee Parameter not Found");
	}

	var data = db.tbl_employee.Find(employee.pk_id);
	data.name = employee.name;
	data.address = employee.address;
	data.date_created = DateTime.Now;
	db.Entry(data).State = EntityState.Modified;
	db.SaveChanges();

}
```
#### After
```c#
public ActionResult EditDataEmployee(tbl_employee employee)
{
	if(employee == null){
		throw new Exception("Employee Parameter not Found");
	}

	try
	{
		var data = db.tbl_employee.Find(employee.pk_id);
		data.name = employee.name;
		data.address = employee.address;
		data.date_created = DateTime.Now;
		db.Entry(data).State = EntityState.Modified;
		db.SaveChanges();
	}
	catch (Exception ex)
	{
		Console.WriteLine(ex.Message);
	}

}
```
Use a try-catch statement for most exception handling.

## Transaction
#### Before
```c#
public ActionResult AddProject(tbl_project project,int id_employee)
{
	try    
	{    
		if(project == null){
			throw new Exception("Project Parameter not Found");
		}
		if(id_employee == null){
			throw new Exception("ID Employee Parameter not Found");
		}

		db.tbl_project.Add(project);
		db.SaveChanges();   

		int projectId = project.pk_id;

		if(projectId == null){
			throw new Exception("ID Project not Found")
		}

		ProjectMembers projMember = new ProjectMembers();
		projMember.fkEmployeeId = id_employee;
		projMember.fkProjectId = projectId;
		db.ProjectMembers.Add(projMember);   
		db.SaveChanges();   
	}    
	catch (Exception ex)    
	{    
		Console.WriteLine(ex.Message);   
	}
}
```
#### After
```c#
public ActionResult EditDataEmployee(tbl_employee employee)
{
	using (DbContextTransaction transaction = db.Database.BeginTransaction( ))    
	{    
		try    
		{    
			if(project == null){
				throw new Exception("Project Parameter not Found");
			}
			if(id_employee == null){
				throw new Exception("ID Employee Parameter not Found");
			}

			db.tbl_project.Add(project);
			db.SaveChanges();   

			int projectId = project.pk_id;

			if(projectId == null){
				throw new Exception("ID Project not Found")
			}

			ProjectMembers projMember = new ProjectMembers();
			projMember.fkEmployeeId = id_employee;
			projMember.fkProjectId = projectId;
			db.ProjectMembers.Add(projMember);   
			db.SaveChanges();
			transaction.Commit();
		}    
		catch (Exception ex)    
		{    
			Console.WriteLine(ex.Message);
			transaction.Rollback();
		}    
	}
}
```
A transaction should be used when you need a set of changes to be processed completely to consider the operation complete and valid.

## Application Parameter
#### Before
```c#
public ActionResult Index(string obj)
{
	string applicationName = "Employee Management";
	ViewBag.ApplicationName = applicationName;
	return View();
}	
```
#### After
```c#
public ActionResult Index(string obj)
{
	var applicationName = db.application_parameters.Where(x => x.pk_application_parameters == 4).FirstOrDefault().value;
	ViewBag.ApplicationName = applicationName;
	return View();
}	
```
It's better to store application settings in database than hardcode it, so users can easily alter application settings or overwrite the contents. We don't need to update the application if there is any change in application settings.
